"""Functional tests for all the resources
"""
import json
import os
import tempfile

import pytest
from flask import Response
from flask.testing import FlaskClient

from feedback import create_app, db
from feedback.models import Course, CourseInstance, Review


@pytest.fixture
def client():
    """Test fixture for setting up the tests

    Yields:
        FlaskClient: Flask test client for running the tests
    """
    db_fd, db_fname = tempfile.mkstemp()
    config = {
        "SQLALCHEMY_DATABASE_URI": "sqlite:///" + db_fname,
        "TESTING": True
    }
    app = create_app(config)

    with app.app_context():
        db.create_all()
        _populate_db()

    yield app.test_client()

    db.session.remove()
    os.close(db_fd)
    os.unlink(db_fname)


def _populate_db():
    """Database population method for the tests
    """
    # create three courses
    for i in range(1, 4):
        course = Course(
            id=f"{i}",
            name=f"ohjelmointi-{i}",
            ects=5
        )
        db.session.add(course)
        db.session.commit()
        if i == 1:
            course_instance = CourseInstance(
                course_id="1",  # Foreign key
                teacher="Matti",
                begin_year=2020,
                begin_period=3
            )
            db.session.add(course_instance)
            db.session.commit()
            review = Review(
                course_instance_id="1",
                rating=5,
                pseudonym="lukutoukka",
                review_text="Oli iha jees",
                method="class",
                students_grade=5
            )
            db.session.add(review)
            db.session.commit()


def _get_course_json(number=4):
    """Generates json for course

    Args:
        number (int, optional): Id for generating course json. Defaults to 4.

    Returns:
        Dict[str, Any]: json for course
    """
    return {
        "id": f"{number}",
        "name": f"ohjelmointi-{number}",
        "ects": 5
    }


def _get_instance_json(id=1):
    """Generates json for course instance

    Args:
        id (int, optional): Id for generating course instance json. Defaults to 1.

    Returns:
        Dict[str, Any]: json for course instance
    """
    return {
        "course_id": id,
        "teacher": "Matti",
        "begin_year": 2020,
        "begin_period": 3
    }


def _get_invalid_json(id=1):
    """Generates invalid json for course instance

    Args:
        id (int, optional): Course id for generating course instance json. Defaults to 1.

    Returns:
        Dict[str, Any]: json for course instance
    """
    return {
        "course_id": id,
        "student": "Matti",
        "begin_year": 2020,
        "begin_period": 3
    }


def _get_review_json(course_instance_id=1):
    """Generates json for review

    Args:
        course_instance_id (int, optional): Course instance to generate review json. Defaults to 1.

    Returns:
        Dict[str, Any]: json for course instance
    """
    return {
        "course_instance_id": course_instance_id,
        "rating": 1,
        "pseudonym": "lukutoukka",
        "review_text": "eipä ollukkaa",
        "method": "class",
        "students_grade": 1
    }


class TestCourseCollection(object):
    RESOURCE_URL = "/api/courses/"

    def test_get(self, client: FlaskClient):
        response = client.get(self.RESOURCE_URL)
        assert response.status_code == 200
        body = json.loads(response.data)
        items = body["items"]
        assert len(items) == 3
        for course in items:
            assert "id" in course
            assert "name" in course
            assert "ects" in course

    def test_post(self, client: FlaskClient):
        valid = _get_course_json()

        response: Response = client.post(self.RESOURCE_URL, json=valid)
        assert response.status_code == 201
        assert response.headers["Location"].endswith(
            self.RESOURCE_URL + valid["name"] + "/")
        response = client.get(response.headers["Location"])
        assert response.status_code == 200
        course = json.loads(response.data)
        assert course["id"] == valid["id"]
        assert course["name"] == valid["name"]
        assert course["ects"] == valid["ects"]

    def test_unsupported_post(self, client: FlaskClient):
        response: Response = client.post(self.RESOURCE_URL, data="test")
        assert response.status_code in (400, 415)

    def test_invalid_post(self, client: FlaskClient):
        invalid = _get_course_json().pop("name")

        response: Response = client.post(self.RESOURCE_URL, json=invalid)
        assert response.status_code == 400

    def test_existing_post(self, client: FlaskClient):
        exists = _get_course_json(1)

        response: Response = client.post(self.RESOURCE_URL, json=exists)
        assert response.status_code == 409


class TestCourseItem(object):

    RESOURCE_URL = "/api/courses/ohjelmointi-1/"
    NEW_RESOURCE_URL = "/api/courses/ohjelmointi-4/"

    def test_get(self, client: FlaskClient):
        response: Response = client.get(self.RESOURCE_URL)
        assert response.status_code == 200
        course = json.loads(response.data)
        assert course["id"] == "1"
        assert course["name"] == "ohjelmointi-1"
        assert course["ects"] == 5

    def test_put(self, client: FlaskClient):
        valid = _get_course_json()
        valid["id"] = "1"

        response: Response = client.put(self.RESOURCE_URL, json=valid)
        assert response.status_code == 204
        response = client.get(self.NEW_RESOURCE_URL)
        assert response.status_code == 200
        course = json.loads(response.data)
        assert course["id"] == valid["id"]
        assert course["name"] == valid["name"]
        assert course["ects"] == valid["ects"]

    def test_delete(self, client: FlaskClient):
        response: Response = client.delete(self.RESOURCE_URL)
        assert response.status_code == 204
        response = client.get(self.RESOURCE_URL)
        assert response.status_code == 404

    def test_unsupported_put(self, client: FlaskClient):
        response: Response = client.put(self.RESOURCE_URL, data="test")
        assert response.status_code in (400, 415)

    def test_invalid_put(self, client: FlaskClient):
        invalid = _get_course_json().pop("ects")

        response: Response = client.put(self.RESOURCE_URL, json=invalid)
        assert response.status_code == 400

    def test_conflict_put(self, client: FlaskClient):
        exists = _get_course_json(2)

        response: Response = client.put(self.RESOURCE_URL, json=exists)
        assert response.status_code == 400


class TestCourseInstanceCollection(object):
    RESOURCE_URL = "/api/courses/ohjelmointi-1/instances/"

    def test_get(self, client):
        response = client.get(self.RESOURCE_URL)
        assert response.status_code == 200
        body = json.loads(response.data)
        items = body["items"]
        assert len(items) == 1
        for course_instance in items:
            assert "teacher" in course_instance
            assert "duration" in course_instance
            assert "begin_year" in course_instance
            assert "begin_period" in course_instance

    def test_post(self, client):
        valid = _get_instance_json("2")

        response = client.post(self.RESOURCE_URL, json=valid)
        assert response.status_code == 201
        assert response.headers["Location"].endswith(
            self.RESOURCE_URL + valid["course_id"] + "/")
        response = client.get(response.headers["Location"])
        assert response.status_code == 200
        course_instance = json.loads(response.data)
        assert course_instance["teacher"] == valid["teacher"]
        assert course_instance["begin_year"] == valid["begin_year"]
        assert course_instance["begin_period"] == valid["begin_period"]

    def test_unsupported_post(self, client):
        response = client.post(self.RESOURCE_URL, data="notjson")
        assert response.status_code in (400, 415)

    def test_invalid_post(self, client):
        invalid = _get_invalid_json()

        response = client.post(self.RESOURCE_URL, json=invalid)
        assert response.status_code == 400

    # TODO: this fails as nothing unique is sent
    # def test_existing_post(self, client: FlaskClient):
    #     exists = _get_instance_json("2")

    #     response: Response = client.post(self.RESOURCE_URL, json=exists)
    #     assert response.status_code == 409


class TestCourseInstanceItem(object):
    RESOURCE_URL = "/api/courses/ohjelmointi-1/instances/1/"
    NEW_RESOURCE_URL = "/api/courses/ohjelmointi-3/instances/1/"

    def test_get(self, client):
        response = client.get(self.RESOURCE_URL)
        assert response.status_code == 200
        course_instance = json.loads(response.data)
        assert "teacher" in course_instance
        assert "duration" in course_instance
        assert "begin_year" in course_instance
        assert "begin_period" in course_instance

    def test_put(self, client):
        valid = _get_instance_json(3)

        response = client.put(self.RESOURCE_URL, json=valid)
        assert response.status_code == 204
        response = client.get(self.NEW_RESOURCE_URL)
        assert response.status_code == 200
        course_instance = json.loads(response.data)
        assert course_instance["teacher"] == valid["teacher"]
# TODO: duration not deserialize, cant check
#        assert course_instance["duration"] == valid["duration"]
        assert course_instance["begin_year"] == valid["begin_year"]
        assert course_instance["begin_period"] == valid["begin_period"]

    def test_delete(self, client):
        response = client.delete(self.NEW_RESOURCE_URL)
        assert response.status_code == 204
        response = client.get(self.NEW_RESOURCE_URL)
        assert response.status_code == 404

    def test_unsupported_put(self, client: FlaskClient):
        response: Response = client.put(self.RESOURCE_URL, data="test")
        assert response.status_code in (400, 415)

    def test_invalid_put(self, client: FlaskClient):
        invalid = _get_instance_json().pop("teacher")

        response: Response = client.put(self.RESOURCE_URL, json=invalid)
        assert response.status_code == 400


class TestReviewCollection(object):

    RESOURCE_URL = "/api/courses/ohjelmointi-1/instances/1/reviews/"

    def test_get(self, client):
        response = client.get(self.RESOURCE_URL)
        assert response.status_code == 200
        body = json.loads(response.data)
        items = body["items"]
        for review in items:
            assert "course_instance_id" in review
            assert "rating" in review
            assert "pseudonym" in review
            assert "review_text" in review
            assert "method" in review
            assert "students_grade" in review

    def test_post_valid_request(self, client):
        valid = _get_review_json()

        response = client.post(self.RESOURCE_URL, json=valid)
        assert response.status_code == 201
        assert response.headers["location"].endswith(
            self.RESOURCE_URL + "2/")
        response = client.get(response.headers["location"])
        assert response.status_code == 200
        body = json.loads(response.data)
        assert body["course_instance_id"] == valid["course_instance_id"]
        assert body["rating"] == valid["rating"]
        assert body["pseudonym"] == valid["pseudonym"]
        assert body["review_text"] == valid["review_text"]
        assert body["method"] == valid["method"]
        assert body["students_grade"] == valid["students_grade"]
        assert "date" in body

    def test_validation_error(self, client):
        invalid = _get_review_json().pop("pseudonym")

        response = client.post(self.RESOURCE_URL, json=invalid)
        assert response.status_code == 400

    # def test_unsupported_put(self, client: FlaskClient):
    #     response: Response = client.put(self.RESOURCE_URL, data="test")
    #     assert response.status_code in (400, 405, 415)


class TestReviewItem(object):

    # TODO replace the placeholders with URL of existing courses/instances
    RESOURCE_URL = "/api/courses/ohjelmointi-2/instances/1/reviews/1/"

    def test_get(self, client):
        response: Response = client.get(self.RESOURCE_URL)
        assert response.status_code == 200
        review = json.loads(response.data)
        assert "course_instance_id" in review
        assert "rating" in review
        assert "pseudonym" in review
        assert "review_text" in review
        assert "method" in review
        assert "students_grade" in review

    def test_put_valid_request(self, client):
        valid = _get_review_json()

        response: Response = client.put(self.RESOURCE_URL, json=valid)
        assert response.status_code == 204
        response = client.get(self.RESOURCE_URL)
        assert response.status_code == 200
        body = json.loads(response.data)
        assert body["course_instance_id"] == valid["course_instance_id"]
        assert body["rating"] == valid["rating"]
        assert body["pseudonym"] == valid["pseudonym"]
        assert body["review_text"] == valid["review_text"]
        assert body["method"] == valid["method"]
        assert body["students_grade"] == valid["students_grade"]
        assert "date" in body

    def test_delete_valid(self, client):
        response = client.delete(self.RESOURCE_URL)
        assert response.status_code == 204
        response = client.get(self.RESOURCE_URL)
        assert response.status_code == 404

    def test_unsupported_put(self, client: FlaskClient):
        response: Response = client.put(self.RESOURCE_URL, data="test")
        assert response.status_code in (400, 415)

    def test_validation_error(self, client):
        invalid = _get_review_json().pop("pseudonym")

        response = client.put(self.RESOURCE_URL, json=invalid)
        assert response.status_code == 400
