PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE courses (
	id VARCHAR NOT NULL, 
	name VARCHAR(128) NOT NULL, 
	ects INTEGER NOT NULL, 
	PRIMARY KEY (id), 
	CHECK (ects > -1 AND ects < 31), 
	UNIQUE (name)
);
INSERT INTO courses VALUES('521260B','Program the web',5);
INSERT INTO courses VALUES('285192S','IoT',5);
INSERT INTO courses VALUES('565432B','Fundamentals of math',5);
COMMIT;
