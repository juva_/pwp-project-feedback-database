PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE reviews (
	id INTEGER NOT NULL, 
	course_instance_id INTEGER NOT NULL, 
	rating INTEGER NOT NULL, 
	pseudonym VARCHAR(64) NOT NULL, 
	review_text VARCHAR(500), 
	date DATETIME NOT NULL, 
	method VARCHAR(32), 
	students_grade INTEGER, 
	PRIMARY KEY (id), 
	CHECK (rating > -1 AND rating < 6 AND students_grade > -1 AND students_grade < 6), 
	FOREIGN KEY(course_instance_id) REFERENCES course_instances (id) ON DELETE CASCADE
);
INSERT INTO reviews VALUES(1,1,5,'lukutoukka','Vaikuttaa lupaavalta','2022-03-01 00:00:00.000000','class',NULL);
INSERT INTO reviews VALUES(2,4,5,'lukutoukka','Oli iha jees','2021-05-20 00:00:00.000000',NULL,5);
INSERT INTO reviews VALUES(3,6,5,'studenttt',NULL,'2020-02-15 00:00:00.000000','hybrid',5);
INSERT INTO reviews VALUES(4,1,1,'ffffffff','Never taking this course again','2022-04-19 00:00:00.000000','remote',1);
INSERT INTO reviews VALUES(5,4,5,'flasker','Programming is so easy','2020-11-29 00:00:00.000000','hybrid',1);
INSERT INTO reviews VALUES(6,4,3,'json','Nice','2022-01-03 00:00:00.000000','hybrid',5);
INSERT INTO reviews VALUES(7,5,5,'lukutoukka',NULL,'2020-10-14 00:00:00.000000','hybrid',5);
INSERT INTO reviews VALUES(8,6,5,'lukutoukka',NULL,'2020-12-02 00:00:00.000000','hybrid',5);
COMMIT;
