PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE course_instances (
	id INTEGER NOT NULL, 
	course_id VARCHAR(32) NOT NULL, 
	teacher VARCHAR(64) NOT NULL, 
	duration INTEGER, 
	begin_year INTEGER NOT NULL, 
	begin_period INTEGER NOT NULL, 
	PRIMARY KEY (id), 
	CHECK (duration > 0 AND duration < 5 AND begin_period > -1 AND begin_period < 6), 
	FOREIGN KEY(course_id) REFERENCES courses (id) ON DELETE CASCADE
);
INSERT INTO course_instances VALUES(1,'565432B','Jason',2,2022,3);
INSERT INTO course_instances VALUES(2,'285192S','Ji Son',2,2022,1);
INSERT INTO course_instances VALUES(3,'521260B','Mika',1,2022,3);
INSERT INTO course_instances VALUES(4,'521260B','Tellervo',1,2021,3);
INSERT INTO course_instances VALUES(5,'285192S','Justiina',2,2020,2);
INSERT INTO course_instances VALUES(6,'565432B','Juuso N',2,2020,3);
COMMIT;
