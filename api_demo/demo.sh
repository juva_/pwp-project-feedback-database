#!/bin/bash
promt="----Continue----"
server_url=""

test_courses() {
    echo "Get course collection"
    http -v GET "$server_url/api/courses/"
    read -p "$promt"
    echo "Invalid post to course collection"
    http -v POST "$server_url/api/courses/" < test_put_course.json
    read -p "$promt"
    echo "Valid post to course collection"
    http -v POST "$server_url/api/courses/" < test_post_course.json
    read -p "$promt"
    echo "Get posted course"
    http -v GET "$server_url/api/courses/Math2/"
    read -p "$promt"
    echo "Delete posted course"
    http -v DELETE "$server_url/api/courses/Math2/"
    read -p "$promt"
    echo "Try to get deleted course"
    http -v GET "$server_url/api/courses/Math2/"
    read -p "$promt"
    echo "Get course item"
    http -v GET "$server_url/api/courses/IoT/"
    read -p "$promt"
    echo "Put to course item"
    http -v PUT "$server_url/api/courses/IoT/" < test_put_course.json
    read -p "$promt"
    echo "Get the edited course item"
    http -v GET "$server_url/api/courses/IoT2/"
    read -p "$promt"
}

test_instances() {
    echo "Get course instance collection"
    http -v GET "$server_url/api/courses/IoT2/instances/"
    read -p "$promt"
    echo "Get course instance item"
    http -v GET "$server_url/api/courses/IoT2/instances/5/"
    read -p "$promt"
    echo "Post to course instance collection"
    http -v POST "$server_url/api/courses/IoT2/instances/" < test_post_instance.json
    read -p "$promt"
    echo "Get course instance collection"
    http -v GET "$server_url/api/courses/IoT2/instances/"
    read -p "$promt"
    echo "Edit the course instance"
    http -v PUT "$server_url/api/courses/IoT2/instances/7/" < test_put_instance.json
    read -p "$promt"
    echo "Get the edited course instance"
    http -v GET "$server_url/api/courses/IoT2/instances/7/"
    read -p "$promt"
    echo "Delete the edited course instance"
    http -v DELETE "$server_url/api/courses/IoT2/instances/7/"
    read -p "$promt"
    echo "Try to get the deleted course instance"
    http -v GET "$server_url/api/courses/IoT2/instances/7/"
    read -p "$promt"
}

test_reviews() {
    echo "Get review collection"
    http -v GET "$server_url/api/courses/IoT2/instances/5/reviews/"
    read -p "$promt"
    echo "Post a review"
    http -v POST "$server_url/api/courses/IoT2/instances/5/reviews/" < test_post_review.json
    read -p "$promt"
    echo "Get the posted review item"
    http -v GET "$server_url/api/courses/IoT2/instances/5/reviews/9/"
    read -p "$promt"
    echo "Edit the review"
    http -v PUT "$server_url/api/courses/IoT2/instances/5/reviews/9/" < test_put_review.json
    read -p "$promt"
    echo "Get review collection"
    http -v GET "$server_url/api/courses/IoT2/instances/5/reviews/"
    read -p "$promt"
    echo "Delete the edited review item"
    http -v DELETE "$server_url/api/courses/IoT2/instances/5/reviews/9/"
    read -p "$promt"
}

revert_changes() {
    http PUT "$server_url/api/courses/IoT2/" < revert_put_course.json > /dev/null
    echo "Changes reverted"
}

read -p "Give server URL or leave empty for default: " server_url
if [[ -z "$server_url" ]]
then
    server_url="localhost:5000"
fi

test_courses
test_instances
test_reviews
revert_changes