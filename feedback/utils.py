"""
Utility classes for feedback module

Contains custom URL converters for different models
"""

from werkzeug.exceptions import NotFound
from werkzeug.routing import BaseConverter

from feedback.api import api
from feedback.models import Course, CourseInstance, Review
from feedback.resources.course import CourseCollection, CourseItem
from feedback.resources.course_instance import CourseInstanceCollection, CourseInstanceItem
from feedback.resources.review import ReviewCollection, ReviewItem


class CourseConverter(BaseConverter):
    """
    Converter class for course
    """

    def to_python(self, value: str) -> Course:
        """Converts name of a course to corresponding course object

        Args:
            value (str): Name of the course

        Raises:
            NotFound: Raised if the specified course was not found in the database

        Returns:
            Course: Course object that corresponds to the specified name
        """
        db_course = Course.query.filter_by(name=value).first()
        if db_course is None:
            raise NotFound
        return db_course

    def to_url(self, value: Course) -> str:
        """Converts course to its name for use in URL

        Args:
            value (Course): Course to convert to an URL

        Returns:
            str: Name of the given course
        """
        return str(value.name)


class CourseInstanceConverter(BaseConverter):
    """
    Converter class for course instance
    """

    def to_python(self, value: str) -> CourseInstance:
        """Converts id of a course instance to corresponding course instance object

        Args:
            value (str): Id of a course instance

        Raises:
            NotFound: Raised if the specified course instance was not found in the database

        Returns:
            CourseInstance: Course instance object that corresponds to the specified id
        """
        db_instance = CourseInstance.query.filter_by(id=int(value)).first()
        if db_instance is None:
            raise NotFound
        return db_instance

    def to_url(self, value: CourseInstance) -> str:
        """Converts course instance to its id for use in URL

        Args:
            value (CourseInstance): Course instance to convert to an URL

        Returns:
            str: Id of the given course instance
        """
        return str(value.id)


class ReviewConverter(BaseConverter):
    """
    Converter class for review
    """

    def to_python(self, value: str) -> Review:
        """Converts id of a review to corresponding review object

        Args:
            value (str): Id of a review

        Raises:
            NotFound: Raised if the specified review was not found in the database

        Returns:
            Review: Review object that corresponds to the specified id
        """
        db_review = Review.query.filter_by(id=int(value)).first()
        if db_review is None:
            raise NotFound
        return db_review

    def to_url(self, value: Review) -> str:
        """Converts Review to its id for use in URL

        Args:
            value (Review): Review to convert to an URL

        Returns:
            str: Id of the given review
        """
        return str(value.id)

# This MasonBuilder is from a PWP course.
# https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/exercise-3-api-documentation-and-hypermedia/
class MasonBuilder(dict):
    """
    A convenience class for managing dictionaries that represent Mason
    objects. It provides nice shorthands for inserting some of the more
    elements into the object but mostly is just a parent for the much more
    useful subclass defined next. This class is generic in the sense that it
    does not contain any application specific implementation details.

    Note that child classes should set the *DELETE_RELATION* to the application
    specific relation name from the application namespace. The IANA standard
    does not define a link relation for deleting something.
    """

    DELETE_RELATION = ""

    def add_error(self, title, details):
        """
        Adds an error element to the object. Should only be used for the root
        object, and only in error scenarios.
        Note: Mason allows more than one string in the @messages property (it's
        in fact an array). However we are being lazy and supporting just one
        message.
        : param str title: Short title for the error
        : param str details: Longer human-readable description
        """

        self["@error"] = {
            "@message": title,
            "@messages": [details],
        }

    def add_namespace(self, ns, uri):
        """
        Adds a namespace element to the object. A namespace defines where our
        link relations are coming from. The URI can be an address where
        developers can find information about our link relations.
        : param str ns: the namespace prefix
        : param str uri: the identifier URI of the namespace
        """

        if "@namespaces" not in self:
            self["@namespaces"] = {}

        self["@namespaces"][ns] = {
            "name": uri
        }

    def add_control(self, ctrl_name, href, **kwargs):
        """
        Adds a control property to an object. Also adds the @controls property
        if it doesn't exist on the object yet. Technically only certain
        properties are allowed for kwargs but again we're being lazy and don't
        perform any checking.
        The allowed properties can be found from here
        https://github.com/JornWildt/Mason/blob/master/Documentation/Mason-draft-2.md
        : param str ctrl_name: name of the control (including namespace if any)
        : param str href: target URI for the control
        """

        if "@controls" not in self:
            self["@controls"] = {}

        self["@controls"][ctrl_name] = kwargs
        self["@controls"][ctrl_name]["href"] = href


class FeedbackBuilder(MasonBuilder):
    """
    These are the added controls to the MasonBuilder. View the state diagram
    from the Wiki page of repository.
    """
    def add_control_add_review(self, course: Course, instance: CourseInstance):
        """
        This control is for adding a review to a course instance with the
        POST method.
        """
        self.add_control(
            "feedback:add-review",
            api.url_for(ReviewCollection, course=course, course_instance=instance),
            method="POST",
            encoding="JSON",
            title="Add a review",
            schema=Review.json_schema()
        )

    def add_control_put_review(self, course: Course, instance: CourseInstance, review: Review):
        """
        This control is for modifying an existing review with the PUT method.
        """
        self.add_control(
            "edit",
            api.url_for(ReviewItem, course=course, course_instance=instance, review=review),
            method="PUT",
            encoding="JSON",
            title="Edit a review",
            schema=Review.json_schema()
        )

    def add_control_delete_review(self, course: Course, instance: CourseInstance, review: Review):
        """
        This control is for deleting an existing review with the DELETE
        method.
        """
        self.add_control(
            "feedback:delete-review",
            api.url_for(ReviewItem, course=course, course_instance=instance, review=review),
            method="DELETE",
            title="Delete a review"
        )

    def add_control_add_course_instance(self, course: Course):
        """
        This control is for adding a course instance to a course with the
        POST method.
        """
        self.add_control(
            "feedback:add-course-instance",
            api.url_for(CourseInstanceCollection, course=course),
            method="POST",
            encoding="JSON",
            title="Add a course instance",
            schema=CourseInstance.json_schema()
        )

    def add_control_put_course_instance(self, course: Course, instance: CourseInstance):
        """
        This control is for modifying an existing course instance with the
        PUT method.
        """
        self.add_control(
            "edit",
            api.url_for(CourseInstanceItem, course=course, course_instance=instance),
            method="PUT",
            encoding="JSON",
            title="Edit a course instance",
            schema=CourseInstance.json_schema()
        )

    def add_control_delete_course_instance(self, course: Course, instance: CourseInstance):
        """
        This control is for deleting an existing course instance with the
        DELETE method.
        """
        self.add_control(
            "feedback:delete-course-instance",
            api.url_for(CourseInstanceItem, course=course, course_instance=instance),
            method="DELETE",
            title="Delete a course instance"
        )

    def add_control_add_course(self):
        """
        This control is for adding a course with the POST method.
        """
        self.add_control(
            "feedback:add-course",
            api.url_for(CourseCollection),
            method="POST",
            encoding="JSON",
            title="Add a course",
            schema=Course.json_schema()
        )

    def add_control_put_course(self, course: Course):
        """
        This control is for modifying an existing course with the PUT method.
        since our state diagram implies edit.
        """
        self.add_control(
            "edit",
            api.url_for(CourseItem, course=course),
            method="PUT",
            encoding="JSON",
            title="Edit a course",
            schema=Course.json_schema()
        )

    def add_control_delete_course(self, course: Course):
        """
        This control is for deleting an existing course with the DELETE method.
        """
        self.add_control(
            "feedback:delete-course",
            api.url_for(CourseItem, course=course),
            method="DELETE",
            title="Delete a course"
        )

    def add_control_course_collection(self):
        """
        This control is to get all courses
        """
        self.add_control(
            "collection",
            api.url_for(CourseCollection),
            method="GET",
            title="Get all courses"
        )

    def add_control_course_instance_collection(self, course: Course):
        """
        This control is to get all course instances
        """
        self.add_control(
            "collection",
            api.url_for(CourseInstanceCollection, course=course),
            method="GET",
            title="Get all course instances"
        )

    def add_control_review_collection(self, course: Course, instance: CourseInstance):
        """
        This control is to get all reviews
        """
        self.add_control(
            "collection",
            api.url_for(ReviewCollection, course=course, course_instance=instance),
            method="GET",
            title="Get all reviews"
        )

    def add_control_course_item(self, course: Course):
        """
        This control is to get a single course item
        """
        self.add_control(
            "item",
            api.url_for(CourseItem, course=course),
            method="GET",
            title="Get a single course"
        )

    def add_control_course_instance_item(self, course: Course, instance: CourseInstance):
        """
        This control is to get a single course instance item
        """
        self.add_control(
            "item",
            api.url_for(CourseInstanceItem, course=course, course_instance=instance),
            method="GET",
            title="Get a single course instance"
        )

    def add_control_review_item(self, course: Course, instance: CourseInstance, review: Review):
        """
        This control is to get a single review item
        """
        self.add_control(
            "item",
            api.url_for(ReviewItem, course=course, course_instance=instance, review=review),
            method="GET",
            title="Get a single review"
        )

    def add_control_course_to_instances(self, course: Course):
        """
        This control is to get all instances of a single course
        """
        self.add_control(
            "feedback:instances",
            api.url_for(CourseInstanceCollection, course=course),
            method="GET",
            title="Get all instances of specific course"
        )

    def add_control_instance_to_reviews(self, course: Course, instance: CourseInstance):
        """
        This control is to get all reviews of a single course instance
        """
        self.add_control(
            "feedback:reviews",
            api.url_for(ReviewCollection, course=course, course_instance=instance),
            method="GET",
            title="Get all reviews of specific course instance"
        )

    def add_control_instance_to_course(self, course: Course):
        """
        This control is to go from the course instance to the course
        """
        self.add_control(
            "feedback:course",
            api.url_for(CourseItem, course=course),
            method="GET",
            title="Get course of instance"
        )

    def add_control_review_to_instance(self, course: Course, instance: CourseInstance):
        """
        This control is to go from a review to the instance
        """
        self.add_control(
            "feedback:course-instance",
            api.url_for(CourseInstanceItem, course=course, course_instance=instance),
            method="GET",
            title="Get instance of review"
        )

    def add_control_reviews_to_instance(self, course: Course, instance: CourseInstance):
        """
        This control goes from review collection to instance
        """
        self.add_control(
            "up",
            api.url_for(CourseInstanceItem, course=course, course_instance=instance),
            method="GET",
            title="Get instance of review collection"
        )

    def add_control_instances_to_course(self, course: Course):
        """
        This control goes from instance collection to the course
        """
        self.add_control(
            "up",
            api.url_for(CourseItem, course=course),
            method="GET",
            title="Get course of instance collection"
        )
