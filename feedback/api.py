"""
Resource routing for the API
"""
from flask import Blueprint
from flask_restful import Api

from feedback.resources.course import CourseCollection, CourseItem
from feedback.resources.course_instance import (CourseInstanceCollection,
                                                CourseInstanceItem)
from feedback.resources.review import ReviewCollection, ReviewItem

# Initialize API with blueprint
api_bp = Blueprint("api", __name__, url_prefix="/api")
api = Api(api_bp)

# Course resources
api.add_resource(CourseCollection, "/courses/")
api.add_resource(CourseItem, "/courses/<course:course>/")

# CourseInstance resources
api.add_resource(CourseInstanceCollection,
                 "/courses/<course:course>/instances/")
api.add_resource(
    CourseInstanceItem, "/courses/<course:course>/instances/<course_instance:course_instance>/")

# Review resources
api.add_resource(
    ReviewCollection,
    "/courses/<course:course>/instances/<course_instance:course_instance>/reviews/"
)
api.add_resource(
    ReviewItem,
    "/courses/<course:course>/instances/<course_instance:course_instance>/reviews/<review:review>/"
)
