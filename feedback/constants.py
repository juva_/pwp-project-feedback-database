"""
Project wide global constants
"""
MASON = "application/vnd.mason+json"
NAMESPACE = "feedback"
LINK_RELATIONS_URL = "/feedback/link-relations/"
COURSE_PROFILE = "/profiles/course/"
INSTANCE_PROFILE = "/profiles/instance/"
REVIEW_PROFILE = "/profiles/review/"
