'''
ReviewCollection and ReviewItem resources
'''
import json

from flasgger import swag_from
from flask import Response, request
from flask_restful import Resource
from jsonschema import ValidationError, validate

from feedback import api, constants, db, utils
from feedback.models import Course, CourseInstance, Review



class ReviewCollection(Resource):
    """
    Review collection resource class.

    Has the methods for each supported http method of this resource.
    """

    @swag_from("../doc/review_collection/get.yml")
    def get(self, course: Course, course_instance: CourseInstance):
        """Method for http get method

        Args:
            course (Course): The course the reviews are associated with
            course_instance (CourseInstance): The instance the reviews are associated with

        Returns:
            Response: Flask response object with Mason formatted serialized data
            of the reviews
        """
        body = utils.FeedbackBuilder()
        body.add_namespace(constants.NAMESPACE, constants.LINK_RELATIONS_URL)
        body.add_control(
            "self", api.api.url_for(self, course=course, course_instance=course_instance))

        # Add control for adding reviews
        body.add_control_add_review(course=course, instance=course_instance)

        # Add control for going back to specific instance
        body.add_control_reviews_to_instance(course=course, instance=course_instance)

        items = body["items"] = []

        reviews: list[Review] = Review.query.filter_by(
            course_instance_id=course_instance.id).all()

        for review in reviews:
            item = utils.FeedbackBuilder(review.serialize())
            item.add_control_review_item(
                course=course, instance=course_instance, review=review)
            item.add_control("profile", constants.REVIEW_PROFILE)
            items.append(item)

        return Response(json.dumps(body), status=200, mimetype=constants.MASON)

    @swag_from("../doc/review_collection/post.yml")
    def post(self, course: Course, course_instance: CourseInstance):
        """Method for http post method

        Args:
            course (Course): The course the reviews are associated with
            course_instance (CourseInstance): The instance the reviews are associated with

        Returns:
            Response: Flask response object with appropriate status code
        """
        if not request.json:
            return Response(status=415)
        try:
            validate(request.json, Review.json_schema())
        except ValidationError:
            return "Validation error", 400

        review = Review()
        Review.deserialize(review, request.json)
        review.course_instance = course_instance

        db.session.add(review)
        db.session.commit()

        review_url = api.api.url_for(
            ReviewItem, **{'course': course, 'course_instance': course_instance, 'review': review})
        return Response(status=201, headers={"Location": review_url})


class ReviewItem(Resource):
    """
    Review item resource class.

    Has the methods for each supported http method of this resource.
    """

    @swag_from("../doc/review_item/get.yml")
    def get(self, review: Review, course: Course, course_instance: CourseInstance):
        """Method for http get method

        Args:
            review (Review): The review that is accessed
            course (Course): The instance the review is associated with
            course_instance (CourseInstance): The course the review is associated with

        Returns:
            Response: Flask response object with Mason formatted serialized data
            of the review
        """
        body = utils.FeedbackBuilder(review.serialize())
        body.add_namespace(constants.NAMESPACE, constants.LINK_RELATIONS_URL)
        body.add_control("self", api.api.url_for(
            self, review=review, course=course, course_instance=course_instance
            )
        )

        # Add control for going back to collection
        body.add_control_review_collection(course=course, instance=course_instance)
        # Add control for going to the specific course instance
        body.add_control_review_to_instance(course=course, instance=course_instance)

        # Add control for editing the review
        body.add_control_put_review(course=course, instance=course_instance, review=review)
        # Add control for deleting the review
        body.add_control_delete_review(course=course, instance=course_instance, review=review)

        body.add_control("profile", constants.REVIEW_PROFILE)

        return Response(json.dumps(body), status=200, mimetype=constants.MASON)

    @swag_from("../doc/review_item/put.yml")
    def put(self, review: Review, course: Course, course_instance: CourseInstance):
        """Method for http put method

        Args:
            review (Review): The review that is accessed
            course (Course): The course the review is associated with
            course_instance (CourseInstance): The instance the review is associated with

        Returns:
            Response: Flask response object with appropriate status code
        """
        if not request.json:
            return Response(status=415)
        try:
            validate(request.json, Review.json_schema())
        except ValidationError:
            return "Validation error", 400

        Review.deserialize(review, request.json)
        review.course_instance = course_instance

        db.session.add(review)
        db.session.commit()

        return Response(status=204)

    @swag_from("../doc/review_item/delete.yml")
    def delete(self, review: Review, course: Course, course_instance: CourseInstance):
        """Method for http delete method

        Args:
            review (Review): The review that is accessed
            course (Course): The course the review is associated with
            course_instance (CourseInstance): The instance the review is associated with

        Returns:
            Response: Flask response object with appropriate status code
        """
        db.session.delete(review)
        db.session.commit()
        return Response(status=204)
