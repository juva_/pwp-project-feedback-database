"""
CourseInstanceCollection and CourseInstanceItem resources
"""
import json

from flasgger import swag_from
from flask import Response, request
from flask_restful import Resource
from jsonschema import ValidationError, validate
from werkzeug.exceptions import BadRequest

from feedback import api, constants, db, utils
from feedback.models import Course, CourseInstance


class CourseInstanceCollection(Resource):
    """
    Course instance collection resource class.

    Has the methods for each supported http method of this resource
    """

    @swag_from("../doc/course_instance_collection/get.yml")
    def get(self, course: Course):
        """Method for http get method

        Args:
            course (Course): The course the instances are associated with

        Returns:
            Response: Flask response object with Mason formatted serialized data
            of the course instances
        """
        body = utils.FeedbackBuilder()
        body.add_namespace(constants.NAMESPACE, constants.LINK_RELATIONS_URL)
        body.add_control("self", api.api.url_for(self, course=course))

        # Add control for adding instances
        body.add_control_add_course_instance(course=course)

        # Add control for going back to the specific course
        body.add_control_instances_to_course(course=course)

        items = body["items"] = []

        instances: list[CourseInstance] = CourseInstance.query.filter_by(
            course_id=course.id).all()

        for instance in instances:
            item = utils.FeedbackBuilder(instance.serialize())
            item.add_control_course_instance_item(course=course, instance=instance)
            item.add_control("profile", constants.INSTANCE_PROFILE)
            items.append(item)

        return Response(json.dumps(body), status=200, mimetype=constants.MASON)

    @swag_from("../doc/course_instance_collection/post.yml")
    def post(self, course: Course):
        """Method for http post method

        Args:
            course (Course): The course the instances are associated with

        Raises:
            BadRequest: Schema validation failed

        Returns:
            Response: Flask response object with appropriate status code
        """
        if not request.json:
            return Response(status=415)

        try:
            validate(request.json, CourseInstance.json_schema())
        except ValidationError as error:
            raise BadRequest(description=str(error)) from error

        course_instance = CourseInstance()
        course_instance.deserialize(request.json)
        course_instance.course = course

        db.session.add(course_instance)
        db.session.commit()

        instance_url = api.api.url_for(
            CourseInstanceItem, **{'course': course, 'course_instance': course_instance})
        return Response(status=201, headers={"Location": instance_url})


class CourseInstanceItem(Resource):
    """
    Course instance collection resource class.

    Has the methods for each supported http method of this resource
    """

    @swag_from("../doc/course_instance_item/get.yml")
    def get(self, course: Course, course_instance: CourseInstance):
        """Method for http get method

        Args:
            course (Course): The course the instance is associated with
            course_instance (CourseInstance): The instance that is accessed

        Returns:
            Response: Flask response object with Mason formatted serialized data
            of the course instance
        """
        body = utils.FeedbackBuilder(course_instance.serialize())
        body.add_namespace(constants.NAMESPACE, constants.LINK_RELATIONS_URL)
        body.add_control(
            "self", api.api.url_for(self, course=course, course_instance=course_instance))
        body.add_control("profile", constants.INSTANCE_PROFILE)

        # Add control for going back to collection
        body.add_control_course_instance_collection(course=course)
        # Add control for going to the instances of this course
        body.add_control_instance_to_reviews(course=course, instance=course_instance)
        # Add control for going to the specific course
        body.add_control_instance_to_course(course=course)

        # Add control for editing the instance
        body.add_control_put_course_instance(course=course, instance=course_instance)
        # Add control for deleting the instance
        body.add_control_delete_course_instance(course=course, instance=course_instance)

        return Response(json.dumps(body), status=200, mimetype=constants.MASON)

    @swag_from("../doc/course_instance_item/put.yml")
    def put(self, course: Course, course_instance: CourseInstance):
        """Method for http put method

        Args:
            course (Course): The course the instance is associated with
            course_instance (CourseInstance): The instance that is accessed

        Raises:
            BadRequest: Schema validation failed

        Returns:
            Response: Flask response object with appropriate status code
        """
        if not request.json:
            return Response(status=415)

        try:
            validate(request.json, CourseInstance.json_schema())
        except ValidationError as error:
            raise BadRequest(description=str(error)) from error

        course_instance.deserialize(request.json)
        course_instance.course = course

        db.session.add(course_instance)
        db.session.commit()

        return Response(status=204)

    @swag_from("../doc/course_instance_item/delete.yml")
    def delete(self, course: Course, course_instance: CourseInstance):
        """Method for http delete method

        Args:
            course (Course): The course the instance is associated with
            course_instance (CourseInstance): The instance that is accessed

        Returns:
            Response: Flask response object with appropriate status code
        """
        db.session.delete(course_instance)
        db.session.commit()
        return Response(status=204)
