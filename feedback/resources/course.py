'''
CourseCollection and CourseItem resources
'''
import json

from flasgger import swag_from
from flask import Response, request
from flask_restful import Resource
from jsonschema import ValidationError, validate
from sqlalchemy.exc import IntegrityError

from feedback import api, constants, db, utils
from feedback.models import Course

# CourseCollection resource implements methods GET and POST

class CourseCollection(Resource):
    """
    Course collection resource class.

    Has the methods for each supported http method of this resource
    """

    @swag_from("../doc/course_collection/get.yml")
    def get(self):
        '''
        This method queries all the courses in in the database, serializes them
        and appends them to a list which in then returned upon a get request.

        This method has somewhat been implemented from
        https://github.com/enkwolf/pwp-course-sensorhub-api-example/blob/master/
        sensorhub/resources/sensor.py
        '''
        body = utils.FeedbackBuilder()
        body.add_namespace(constants.NAMESPACE, constants.LINK_RELATIONS_URL)
        body.add_control("self", api.api.url_for(CourseCollection))
        body.add_control_add_course()
        items = body["items"] = []

        # Adding the courses into a dictionary with their respective info
        for course in Course.query.all():
            item = utils.FeedbackBuilder(Course.serialize(course))
            item.add_control_course_item(course=course)
            item.add_control("profile", constants.COURSE_PROFILE)
            items.append(item)

        return Response(json.dumps(body), status=200, mimetype=constants.MASON)


    @swag_from("../doc/course_collection/post.yml")
    def post(self):
        '''
        This method posts the JSON data it recieves if it is JSON, it mathces
        the JSONSCHEMA and if the database does not already have an item with
        the same course ID. If it successfully is able to add tha data,
        it returns status code 201. This method is also built from
        https://github.com/enkwolf/pwp-course-sensorhub-api-example/blob/master/
        sensorhub/resources/sensor.py
        '''
        if not request.json:
            return Response(status=415)
        try:
            validate(request.json, Course.json_schema())
        except ValidationError:
            return "Validation error", 400

        course = Course()
        Course.deserialize(course, request.json)

        try:
            db.session.add(course)
            db.session.commit()
        # All exceptions while trying to POST new course
        except IntegrityError:
            return "Item already exists", 409
        header_dict = {"Location": api.api.url_for(CourseItem, course=course)}
        return Response(status=201, headers=header_dict)


class CourseItem(Resource):
    """
    Course item resource class.

    Has the methods for each supported http method of this resource
    """

    @swag_from("../doc/course_item/get.yml")
    def get(self, course: Course):
        """Method for http get method

        Args:
            course (Course): The course which is accessed

        Returns:
            Response: Flask response object with Mason formatted serialized data of the course
        """
        body = utils.FeedbackBuilder(course.serialize())
        body.add_namespace(constants.NAMESPACE, constants.LINK_RELATIONS_URL)
        body.add_control("self", api.api.url_for(CourseItem, course=course))
        body.add_control("profile", constants.COURSE_PROFILE)

        # Add control for going back to collection
        body.add_control_course_collection()
        # Add control for going to the instances of this course
        body.add_control_course_to_instances(course)

        # Add control for editing the course
        body.add_control_put_course(course)
        # Add control for deleting the course
        body.add_control_delete_course(course)

        return Response(json.dumps(body), status=200, mimetype=constants.MASON)

    @swag_from("../doc/course_item/put.yml")
    def put(self, course: Course):
        """Method for http put method

        Args:
            course (Course): The course which is accessed

        Returns:
            Response: Flask response object with appropriate status code
        """
        if not request.json:
            return Response(status=415)
        try:
            validate(request.json, Course.json_schema())
        except ValidationError:
            return "Validation Error", 400

        course.deserialize(request.json)

        try:
            db.session.add(course)
            db.session.commit()
        except IntegrityError:
            return "Course already exists", 400
        return Response(status=204)

    @swag_from("../doc/course_item/delete.yml")
    def delete(self, course):
        """Method for http delete method

        Args:
            course (Course): The course which is accessed

        Returns:
            Response: Flask response object with appropriate status code
        """
        db.session.delete(course)
        db.session.commit()
        return Response(status=204)
