"""
Models for the feedback database
"""
from datetime import datetime
from json import load
from pathlib import Path

import click
from flask.cli import with_appcontext
from sqlalchemy import CheckConstraint, event
from sqlalchemy.engine import Engine

from feedback import db

'''
Since SQLite does not enforece foreign key constraints, the follwoing method
was created. At least version 3.6.19 of SQLite must be used for this method.

Source: https://docs.sqlalchemy.org/en/20/dialects/sqlite.html
'''
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


'''
This model is for the course, which is at the top of our database hierarchy.
It checks if the ECTS for the course are between 1 and 30(limits have been
created from ECTS from courses at the University Of Oulu). This model only has
the course ID, name and ECTS. Each course can have multiple course_instances.
All of the three columns are mandatory and the JSONSCHEMA is the firsthand
checker.

Built from https://github.com/enkwolf/pwp-course-sensorhub-api-example/blob/
master/sensorhub/models.py
'''
class Course(db.Model):

    __tablename__ = "courses"

    # Constraints for amount of study credits
    __table_args__ = (
        CheckConstraint("ects > -1 AND ects < 31"),
        {})

    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(128), unique=True, nullable=False)
    ects = db.Column(db.Integer, nullable=False)  # Study Credits

    course_instances = db.relationship(
        "CourseInstance", back_populates="course", cascade="all, delete", passive_deletes=True)

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "ects": self.ects
        }

    def deserialize(self, doc):
        self.id = doc["id"]
        self.name = doc["name"]
        self.ects = doc["ects"]

    @staticmethod
    def json_schema():
        schema = {
            "type": "object",
            "required": ["id", "name", "ects"]
        }
        props = schema["properties"] = {}
        props["id"] = {
            "description": "ID of course",
            "type": "string"
        }
        props["name"] = {
            "description": "Name of course",
            "type": "string"
        }
        props["ects"] = {
            "description": "Number of study credits for course",
            "type": "number"
        }
        return schema

'''
This model is similar to the previous on in how it works. It checks constraints
for duration and begin period. It can serialize and deseralize and the
JSONSCHEMA validates the request input.

Built from https://github.com/enkwolf/pwp-course-sensorhub-api-example/blob/
master/sensorhub/models.py
'''
class CourseInstance(db.Model):
    """Model for course instance"""

    __tablename__ = "course_instances"

    # Constraints for duration and begin_period
    __table_args__ = (
        CheckConstraint("duration > 0 AND duration < 5 AND "
                        + "begin_period > -1 AND begin_period < 6"),
        {})

    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(
        db.String(32), db.ForeignKey("courses.id", ondelete="CASCADE"), nullable=False)
    teacher = db.Column(db.String(64), nullable=False)
    duration = db.Column(db.Integer, default=1)
    begin_year = db.Column(db.Integer, nullable=False)
    begin_period = db.Column(db.Integer, nullable=False)

    course = db.relationship("Course", back_populates="course_instances")
    reviews = db.relationship(
        "Review", back_populates="course_instance", cascade="all, delete", passive_deletes=True)

    def serialize(self):
        return {
            "teacher": self.teacher,
            "duration": self.duration,
            "begin_year": self.begin_year,
            "begin_period": self.begin_period
        }

    def deserialize(self, doc: dict[str, any]):
        self.teacher = doc["teacher"]
        self.begin_year = doc["begin_year"]
        self.begin_period = doc["begin_period"]
        # Can be None, defaults to 1 in db
        self.duration = doc.get("duration")

    @staticmethod
    def json_schema():
        schema = {
            "type": "object",
            "required": ["teacher", "begin_year", "begin_period"]
        }
        props = schema["properties"] = {}
        props["teacher"] = {
            "description": "Course instance teacher's name",
            "type": "string"
        }
        props["duration"] = {
            "description": "Course instance duration",
            "type": "number"
        }
        props["begin_year"] = {
            "description": "Course instance begin year",
            "type": "number"
        }
        props["begin_period"] = {
            "description": "Course instance begin period",
            "type": "number"
        }

        return schema


class Review(db.Model):
    """Model for course review"""

    __tablename__ = "reviews"

    # Constraints for rating and students grade
    __table_args__ = (
        CheckConstraint("rating > -1 AND rating < 6 AND "
                        + "students_grade > -1 AND students_grade < 6"),
        {})

    id = db.Column(db.Integer, primary_key=True)
    course_instance_id = db.Column(
        db.Integer, db.ForeignKey("course_instances.id", ondelete="CASCADE"), nullable=False)
    rating = db.Column(db.Integer, nullable=False)
    pseudonym = db.Column(db.String(64), nullable=False)
    review_text = db.Column(db.String(500), nullable=True)
    date = db.Column(db.DateTime, default=datetime.now())
    method = db.Column(db.String(32), nullable=True)
    students_grade = db.Column(db.Integer, nullable=True)

    course_instance = db.relationship(
        "CourseInstance", back_populates="reviews")

    def serialize(self):
        return {
            "course_instance_id": self.course_instance_id,
            "rating": self.rating,
            "pseudonym": self.pseudonym,
            "review_text": self.review_text,
            "date": str(self.date.strftime("%d/%m/%Y")),
            "method": self.method,
            "students_grade": self.students_grade
        }

    def deserialize(self, doc):
        self.rating = doc["rating"]
        self.pseudonym = doc["pseudonym"]
        self.review_text = doc["review_text"]
#        self.date = doc["date"]
        self.method = doc["method"]
        self.students_grade = doc["students_grade"]

    @staticmethod
    def json_schema():
        schema = {
            "type": "object",
            "required": ["rating", "pseudonym"]
        }
        props = schema["properties"] = {}
        props["rating"] = {
            "description": "Students rating for course",
            "type": "number"
        }
        props["pseudonym"] = {
            "description": "Students pseudonym",
            "type": "string"
        }
        props["review_text"] = {
            "description": "Review text",
            "type": ["string", "null"]
        }
        props["method"] = {
            "description": "Course implementation. Physical, hybrid, Zoom?",
            "type": ["string", "null"]
        }
        props["students_grade"] = {
            "description": "Student's grade",
            "type": ["number", "null"]
        }
        return schema


@click.command("init-db")
@with_appcontext
def init_db_command():
    db.create_all()


@click.command("populate-db")
@with_appcontext
def populate_db_command():
    """
    This command populates the database directly
    with filler data from the dummydata.json file.
    No parameters and returns nothing.
    It has separate loops for the courses, course instances and reviews.
    """

    db.create_all()  # Doesn't hurt to do this just in case

    # Load json from file (using directory separator of the current os)
    with open(Path("db", "dymmydata.json"), encoding='utf-8') as f:
        data = load(f)

    for course in data["courses"]:
        new_course = Course(
            id=course["id"],  # PK eg. "521260S"
            name=course["name"],
            ects=course["ects"]
        )
        # Add to database
        db.session.add(new_course)
        db.session.commit()

    for course_instance in data["courseinstances"]:
        new_course_instance = CourseInstance(
            # id needs to be defined to apply the relations from the .json
            id=course_instance["id"],  # PK
            course_id=course_instance["course_id"],  # Foreign key
            teacher=course_instance["teacher"],
            duration=course_instance["duration"],
            begin_year=course_instance["begin_year"],
            begin_period=course_instance["begin_period"]
        )
        # Add to database
        db.session.add(new_course_instance)
        db.session.commit()

    for review in data["reviews"]:
        new_review = Review(
            course_instance_id=review["course_instance_id"],  # Foreign key
            rating=review["rating"],
            pseudonym=review["pseudonym"],
            review_text=review["review_text"],
            # convert ISO8601 string to datetime
            date=datetime.fromisoformat(review["date"]),
            method=review["method"],
            students_grade=review["students_grade"]
        )
        # Add to database
        db.session.add(new_review)
        db.session.commit()
