"""
Init file for feedback module

Initializes Flask app and database. Contains logic for determining the path for the database file
"""

import inspect
import json
import os
from pathlib import PurePath

from flask import Flask, Response
from flask_sqlalchemy import SQLAlchemy
from flasgger import Swagger

db = SQLAlchemy()


def create_app(test_config=None):
    """
    App factory method

    Initializes the flask app with specified config

    Args:
        test_config (Map, optional): Flask app config mapping. Defaults to None.

    Returns:
        app Flask: Initialized Flask app
    """
    app = Flask(__name__, static_folder="static")

    # Check if running with test configuration
    if test_config is None:
        # TODO Make this less hacky for deployment
        # Get os dependant absolute path
        file_location = PurePath((inspect.getfile(inspect.currentframe())))
        basedir = file_location.parents[1]  # "Navigate" 1 directory up
        # Concatenate the database URI
        db_uri = "sqlite:///" + os.path.join(basedir, "db", "test.db")

        app.config["SQLALCHEMY_DATABASE_URI"] = db_uri
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
        app.config["SWAGGER"] = {
            "title": "Feedback database",
            "openapi": "3.0.3",
            "uiversion": 3,
            'ui_params': {
                'operationsSorter': 'method' # Sorts the documentation by http method
            }
        }

    else:
        app.config.from_mapping(test_config)

    db.init_app(app)

    # Initialize flasgger
    swagger = Swagger(app, template_file="doc/feedback.yml")

    from feedback.utils import (CourseConverter, CourseInstanceConverter,
                                ReviewConverter, FeedbackBuilder)

    from . import api, models, constants

    #Register cli commands
    app.cli.add_command(models.init_db_command)
    app.cli.add_command(models.populate_db_command)

    #Register URL converters
    app.url_map.converters["course"] = CourseConverter
    app.url_map.converters["course_instance"] = CourseInstanceConverter
    app.url_map.converters["review"] = ReviewConverter

    app.register_blueprint(api.api_bp)

    @app.route(constants.LINK_RELATIONS_URL)
    def send_link_relations():
        return app.send_static_file("html/link_relations.html")

    @app.route("/profiles/<profile>/")
    def send_profiles(profile: str):
        return app.send_static_file(f"html/profiles/{profile}.html")

    @app.route("/api/")
    def return_entry_point():
        body = FeedbackBuilder()
        body.add_namespace(constants.NAMESPACE, constants.LINK_RELATIONS_URL)
        body.add_control_course_collection()
        return Response(json.dumps(body), status=200, mimetype=constants.MASON)

    return app
