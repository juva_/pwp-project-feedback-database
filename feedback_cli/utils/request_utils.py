""" Utility methods for handling API requests and responses
"""

from typing import Tuple, Any

def filter_controls(doc: dict[str, Any]) -> Tuple[dict[str, Any], dict[str, Any]]:
    """ Filters and splits controls and data from a Mason document

    Args:
        doc (dict[str, Any]): Mason document to be filtered

    Returns:
        Tuple(dict, dict): Tuple containing two dictionaries.
        First has the controls and second the data from the filtered Mason document.
    """

    controls = doc.pop("@controls")

    # Discard any possible error messages
    if "@error" in doc:
        _ = doc.pop("@error")

    # Discard the namespaces
    if "@namespaces" in doc:
        _ = doc.pop("@namespaces")

    data = doc

    return controls, data
