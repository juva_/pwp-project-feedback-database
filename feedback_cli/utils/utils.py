'''Utility method for other than request or draw utils
'''
def generate_starting_date(year, period):
    """ Generates a course staring date string

    Args:
        year (int): Year as an integer
        period (int): Starting period an integer

    Returns:
        str: formatted staring date string
    """
    starting_date = ""
    month = ""
    match period:
        case 0:
            month = "8"
        case 1:
            month = "9"
        case 2:
            month = "11"
        case 3:
            month = "1"
        case 4:
            month = "3"
        case 5:
            month = "5"
    starting_date = str(year) + "-" + month
    return starting_date
