""" Utility methods for formatting prints
"""

def format_header(header_text: str) -> str:
    """ Formats a header string

    Args:
        header_text (str): Text to be included in the header

    Returns:
        str: Formatted header string
    """
    # Margin for left and right side of the text
    margin = 4

    # Calculate the total header width
    header_width = len(header_text) + (2 * margin) + 2

    # Center the text
    text_row = header_text.center(header_width - 2)

    # Add border to text row
    text_row = '#' + text_row
    text_row = text_row + '#'

    # Build the header
    header = header_width * '#' + "\n" + text_row + "\n" + header_width * '#'
    return header

def format_paged_menu(page_number: int, pages: int, menu_items: list[str]) -> str:
    """ Formats a menu string for views with pages

    Args:
        page_number (int): The number of current page
        pages (int): Amount of pages
        menu_items (list[str]): List containing the menu element strings

    Returns:
        str: Formatted menu string
    """
    page_row = f"Page {page_number} of {pages} (N)ext/(P)revious"

    menu_row = f"{menu_items.pop(0)}"
    for item in menu_items:
        menu_row = menu_row + ", " + item

    menu_width = len(menu_row)

    # Build the menu
    menu = menu_width * '=' + "\n" + page_row + "\n" + menu_width * '-' + "\n" +\
        menu_row + "\n" + menu_width * '='
    return menu

def format_menu(menu_items: list[str]) -> str:
    """ Formats a menu string

    Args:
        menu_items (list[str]): List containing the menu element strings

    Returns:
        str: Formatted menu string
    """
    menu_row = f"{menu_items.pop(0)}"
    for item in menu_items:
        menu_row = menu_row + ", " + item

    menu_width = len(menu_row)

    # Build the menu
    menu = menu_width * '=' + "\n" + menu_row + "\n" + menu_width * '='
    return menu

def format_table(menu_items: list[str]) -> str:
    '''Generates table

    Args:
        menu_items (list[str]): List containing the menu element strings

    Returns:
        A basic table which has no outer side lines

    Example output:
        ------------------------------
         # | Starting month | Teacher
        ------------------------------
    '''
    outline = ""
    text = ""
    for i, word in enumerate(menu_items):
        if (i > 0) and (i != len(menu_items)):
            text += "|"
        text += " " + word + " "
    outline = "-" * len(text)
    table = outline + "\n" + text + "\n" + outline
    return table

def format_details_card(items: list[str]) -> str:
    """ Formats a detail card string

    Args:
        items (list[str]): List containing items to be included in the card

    Returns:
        str: String containing the items formatted into a card
    """
    # Find the max length of string in items
    item_max_width = max(len(item) for item in items)

    card = ""
    card = card + '| ' + items.pop(0).ljust(item_max_width) + ' |' + "\n"
    for item in items:
        card = card + '|' + '-' * (item_max_width + 2) + '|' + "\n"
        card = card + '| ' + item.ljust(item_max_width) + ' |' + "\n"

    return card
