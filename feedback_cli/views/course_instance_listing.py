'''View class for a course's instance list
'''

from requests import Session

from models.models import Course, CourseInstance
from utils.draw_utils import format_header, format_menu, format_table
from utils.request_utils import filter_controls
from utils.utils import generate_starting_date
from .view import View


class CourseInstanceListingView(View):
    '''View for presenting a course's instance list
    '''
    def __init__(self) -> None:
        self.course_instance_list = []
        self.course_instance_paths = {}
        self.controls = {}

    def draw(self, **kwargs):
        current_course: Course = kwargs.get("current_course")

        # Reset console https://en.wikipedia.org/wiki/ANSI_escape_code
        print("\033c", end="")
        print(format_header(f"{current_course.name} INSTANCES"))
        table_to_format = ["#", "Starting month", "Teacher"]
        print(format_table(table_to_format))
        for index, course_instance in enumerate(self.course_instance_list):
            starting_month = generate_starting_date(
                course_instance.begin_year,
                course_instance.begin_period
            )
            data = "(" + str(index) + ")" + " " + starting_month + ", " + course_instance.teacher
            print(data)
        print(format_menu(["Select course instance (number)", "(B)ack", "(Q)uit"]))

    def request(self, url: str, session: Session):
        response = session.get(url)
        self.controls, data = filter_controls(response.json())
        self.course_instance_list.clear()
        for item in data["items"]:
            item_controls, item_data = filter_controls(item)
            course_instance = CourseInstance()
            course_instance.deserialize(item_data)
            self.course_instance_list.append(course_instance)
            # Use object id as reference
            self.course_instance_paths[id(course_instance)] = item_controls["item"]["href"]
