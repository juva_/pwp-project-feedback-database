""" Contains course listing view class
"""

from requests import Session

from models.models import Course
from utils.request_utils import filter_controls
from utils.draw_utils import format_header, format_menu
from .view import View

class CourseListingView(View):
    """ View for presenting the course listing
    """
    def __init__(self) -> None:
        self.course_list = []
        self.course_paths = {}
        self.controls = {}

    def draw(self, **kwargs):
        # Reset console https://en.wikipedia.org/wiki/ANSI_escape_code
        print("\033c", end="")
        print(format_header("ALL COURSES"))
        for index, course in enumerate(self.course_list):
            print(f"({index}) {course.name} ({course.id})")
        print(format_menu(["Select course (number)", "(H)elp", "(Q)uit"]))


    def request(self, url: str, session: Session):
        response = session.get(url)
        self.controls, data = filter_controls(response.json())
        self.course_list.clear()
        for item in data["items"]:
            item_controls, item_data = filter_controls(item)
            course = Course()
            course.deserialize(item_data)
            self.course_list.append(course)
            self.course_paths[course.id] = item_controls["item"]["href"]
