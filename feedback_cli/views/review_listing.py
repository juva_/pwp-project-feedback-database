'''View class for a course instance's review
'''

from requests import Session
from utils.draw_utils import format_header, format_menu, format_table
from utils.request_utils import filter_controls
from utils.utils import generate_starting_date
from models.models import Course, CourseInstance, Review

from .view import View

class ReviewListingView(View):
    '''Class for presenting a course instance's review list
    '''

    def __init__(self) -> None:
        self.review_list = []
        self.review_paths = {}
        self.controls = {}

    def draw(self, **kwargs):
        current_course: Course = kwargs.get("current_course")
        current_instance: CourseInstance = kwargs.get("current_instance")

        # Reset console https://en.wikipedia.org/wiki/ANSI_escape_code
        print("\033c", end="")
        starting_month = generate_starting_date(
            current_instance.begin_year,
            current_instance.begin_period
        )
        print(format_header(f"{current_course.name}, {starting_month} REVIEWS"))
        table_to_format = ["#", "Date", "Name", "Rating"]
        print(format_table(table_to_format))
        for index, review in enumerate(self.review_list):
            print(f"({index}) {review.review_date}, {review.pseudonym}, {review.rating}/5 \n")
        print(format_menu(["Select review (number)", "(B)ack", "(Q)uit"]))


    def request(self, url: str, session: Session):
        response = session.get(url)
        self.controls, data = filter_controls(response.json())
        self.review_list.clear()
        for item in data["items"]:
            item_controls, item_data = filter_controls(item)
            review = Review()
            review.deserialize(item_data)
            self.review_list.append(review)
            # Use object id as reference
            self.review_paths[id(review)] = item_controls["item"]["href"]
