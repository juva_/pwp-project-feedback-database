'''Contains manual class
'''

from requests import Session
from utils.draw_utils import format_header, format_menu
from .view import View

class ManualView(View):
    '''View for presenting the manual
    '''

    def __init__(self) -> None:
        pass

    def draw(self, **kwargs):
        # Reset console https://en.wikipedia.org/wiki/ANSI_escape_code
        print("\033c", end="")
        print(format_header("MANUAL"))
        print("\n")
        print(format_menu(["(L)ist courses", "(Q)uit"]))

    def request(self, url: str, session: Session):
        pass
