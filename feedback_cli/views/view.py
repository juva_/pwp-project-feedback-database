""" Contains base view class
"""

from abc import ABC, abstractmethod

from requests import Session

class View(ABC):
    """ Base view class
    """

    @abstractmethod
    def draw(self, **kwargs):
        """ Draws the view
        """

    @abstractmethod
    def request(self, url: str, session: Session):
        """ Requests required information from the API

        Args:
            url (str): URL that should be used for the GET request
            session (Session): Requests session that should be used for the GET request
        """
