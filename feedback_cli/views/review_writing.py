""" Contains review writing view class
"""

from requests import Session

from models.models import Course, CourseInstance, Review
from utils.draw_utils import format_header, format_menu
from utils.request_utils import filter_controls
from utils.utils import generate_starting_date
from .view import View

class ReviewWritingView(View):
    """ View for writing reviews
    """
    property_numbers = {
        1: "pseudonym",
        2: "students_grade",
        3: "method",
        4: "rating",
        5: "review_text"
     }

    def __init__(self) -> None:
        self.controls = {}
        self.review = Review()
        self.properties = {}
        self.required = []
        self.editing = False

    def draw(self, **kwargs):
        current_course: Course = kwargs.get("current_course")
        current_instance: CourseInstance = kwargs.get("current_instance")
        property_number: int = kwargs.get("current_property_number", None)

        instance_date = generate_starting_date(
            current_instance.begin_year, current_instance.begin_period)

        # Reset console https://en.wikipedia.org/wiki/ANSI_escape_code
        print("\033c", end="")
        print(format_header(f"{current_course.name} {instance_date} REVIEW"))
        print()

        for number, property_name in self.property_numbers.items():
            print(f"({number}) {self.properties[property_name]['prompt']}", end="")

            if property_name in self.required:
                print('*', end="")

            print(": ", end="")

            attribute = getattr(self.review, property_name, None)
            if attribute is not None:
                print(attribute)
            else:
                print()

        if not self.editing:
            print(format_menu(["Select property (number)", "(P)ost", "(C)ancel", "(Q)uit"]))
        else:
            current_property = self.properties[self.property_numbers[property_number]]
            prompt = current_property['prompt']
            property_type = current_property['type']
            if isinstance(property_type, list):
                property_type = property_type[0]
            menu_prompt = f"Give new value for: ({property_number}) {prompt} ({property_type})"
            print(format_menu([menu_prompt]))

    def request(self, url: str, session: Session):
        response = session.get(url)
        self.controls, _ = filter_controls(response.json())
        schema = self.controls["feedback:add-review"]["schema"]
        self.properties = schema["properties"]
        self.properties["pseudonym"]["prompt"] = "Your name"
        self.properties["students_grade"]["prompt"] = "Your grade"
        self.properties["method"]["prompt"] = "Method"
        self.properties["rating"]["prompt"] = "Rating"
        self.properties["review_text"]["prompt"] = "Review text"
        self.required = schema["required"]
