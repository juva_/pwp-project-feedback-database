""" Contains course details view class
"""

from requests import Session

from models.models import Course, CourseInstance
from utils.draw_utils import format_header, format_menu, format_details_card
from utils.request_utils import filter_controls
from utils.utils import generate_starting_date
from .view import View

class CourseInstanceDetailsView(View):
    """ View for presenting course instance's details
    """

    def __init__(self) -> None:
        self.controls = {}

    def draw(self, **kwargs):
        current_course: Course = kwargs.get("current_course")
        current_instance: CourseInstance = kwargs.get("current_instance")

        instance_date = generate_starting_date(
            current_instance.begin_year, current_instance.begin_period)

        # Reset console https://en.wikipedia.org/wiki/ANSI_escape_code
        print("\033c", end="")
        print(format_header(f"{instance_date}, {current_instance.teacher}"))
        card_items = [
            f"Course Name: {current_course.name}",
            f"Course ID: {current_course.id}",
            f"ECTS: {current_course.ects}",
            f"Teacher: {current_instance.teacher}",
            f"Duration: {current_instance.duration}",
            f"Begin Year: {current_instance.begin_year}",
            f"Begin Period: {current_instance.begin_period}"
            ]
        print()
        print(format_details_card(card_items))
        print(format_menu(["(A)dd review", "(L)ist reviews", "(B)ack", "(Q)uit"]))


    def request(self, url: str, session: Session):
        response = session.get(url)
        self.controls, _ = filter_controls(response.json())
