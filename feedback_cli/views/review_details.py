'''Contains review details class
'''

from requests import Session

from models.models import Course, CourseInstance, Review
from utils.draw_utils import format_header, format_menu
from utils.request_utils import filter_controls
from .view import View

class ReviewDetailsView(View):
    """ View for presenting review's details
    """

    def __init__(self) -> None:
        self.controls = {}

    def draw(self, **kwargs):
        current_course: Course = kwargs.get("current_course")
        current_instance: CourseInstance = kwargs.get("current_instance")
        current_review: Review = kwargs.get("current_review")

        # Reset console https://en.wikipedia.org/wiki/ANSI_escape_code
        print("\033c", end="")
        print(format_header(f"{current_course.name} {current_instance.teacher} REVIEW"))

        mid_bar = "-" * 40

        grade = current_review.students_grade
        date = current_review.review_date
        text1 = (f"{date}\nName: {current_review.pseudonym}\nRating: {current_review.rating} / 5")
        text2 = (f"Student's grade: {grade} / 5\nMethod: {current_review.method}")
        text3 = (f"Review:\n{current_review.review_text}")
        print(f"{text1}\n{mid_bar}\n{text2}\n{mid_bar}\n{text3}")
        print(format_menu(["(L)ist reviews", "(E)dit", "(D)elete", "(Q)uit"]))


    def request(self, url: str, session: Session):
        response = session.get(url)
        self.controls, _ = filter_controls(response.json())
