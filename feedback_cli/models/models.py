""" Client side representations of the API database models
"""

from dataclasses import dataclass
from datetime import date
from typing import Any

@dataclass
class Course():
    """ Model representing a course
    """
    id: str
    name: str
    ects: int

    # Default constructor
    def __init__(self):
        pass

    def serialize(self):
        """ Transforms the course object to a dictionary format
        """
        return {
            "id": self.id,
            "name": self.name,
            "ects": self.ects
        }

    def deserialize(self, doc):
        """ Transforms dictionary representing a course to an object
        """
        self.id = doc["id"]
        self.name = doc["name"]
        self.ects = doc["ects"]

@dataclass
class CourseInstance():
    """ Model representing a course instance
    """
    teacher: str
    duration: int
    begin_year: int
    begin_period: int

    # Default constructor
    def __init__(self):
        pass

    def serialize(self):
        """ Transforms the course instance object to a dictionary format
        """
        return {
            "teacher": self.teacher,
            "duration": self.duration,
            "begin_year": self.begin_year,
            "begin_period": self.begin_period
        }

    def deserialize(self, doc: dict[str, any]):
        """ Transforms dictionary representing a course instance to an object
        """
        self.teacher = doc["teacher"]
        self.begin_year = doc["begin_year"]
        self.begin_period = doc["begin_period"]
        self.duration = doc["duration"]

@dataclass
class Review():
    """ Model representing a review
    """
    rating: int
    pseudonym: str
    review_text: str
    review_date: date
    method: str
    students_grade: int

    # Default constructor
    def __init__(self):
        self.rating = None
        self.pseudonym = None
        self.review_text = None
        self.review_date = None
        self.method = None
        self.students_grade = None

    def serialize(self):
        """ Transforms the review object to a dictionary format
        """
        return {
            "rating": self.rating,
            "pseudonym": self.pseudonym,
            "review_text": self.review_text,
            "method": self.method,
            "students_grade": self.students_grade
        }

    def deserialize(self, doc: dict[str, Any]):
        """ Transforms dictionary representing a review to an object
        """
        self.rating = doc.get("rating")
        self.pseudonym = doc["pseudonym"]
        self.review_text = doc["review_text"]
        self.review_date = doc["date"]
        self.method = doc["method"]
        self.students_grade = doc["students_grade"]
