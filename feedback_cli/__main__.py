""" Main module of the client
"""
import argparse
import datetime
from enum import Enum
from os import getenv

import requests
from dotenv import load_dotenv

from views import (
    course_listing,
    course_instance_listing,
    course_instance_details,
    manual,
    review_listing,
    review_details,
    review_writing,
    review_editing
    )
from views import view as base_view
from utils.request_utils import filter_controls


class ScreenStates(Enum):
    """ Enum for possible screen states
    """
    EXIT = 0
    COURSE_LISTING = 1
    INSTANCE_LISTING = 2
    INSTANCE_DETAILS = 3
    REVIEW_LISTING = 4
    REVIEW_DETAILS = 5
    REVIEW_WRITING = 6
    REVIEW_EDITING = 7
    MANUAL = 8

class ScreenStateMachine():
    """ State machine for handling screen state
    """
    def __init__(self) -> None:
        self.state = ScreenStates.COURSE_LISTING

    def change_state(self, new_state: ScreenStates):
        """ Changes the state to the specified state

        Args:
            new_state (ScreenStates): New state that should be used
        """
        self.state = new_state

class IllegalOption(Exception):
    """ Raised for options that are not available in the current screen
    """

    def __init__(self, option, message="\033[31m Illegal option:") -> None:
        self.option = option
        self.message = message
        super().__init__(self.message)

def main():
    """ Main method of the client
    """
    with requests.Session() as session:
        response = session.get(SERVER_URL + "/api/")
        body = response.json()
        view_endpoint = body["@controls"]["collection"]["href"]

        while True:
            try:
                match SCREEN_STATE.state:
                    case ScreenStates.COURSE_LISTING:
                        view = course_listing.CourseListingView()
                        view.request(SERVER_URL + view_endpoint, session)
                        view.draw()
                        option = input("> ")
                        view_endpoint = handle_option(option, view, session)
                    case ScreenStates.INSTANCE_LISTING:
                        view = course_instance_listing.CourseInstanceListingView()
                        view.request(SERVER_URL + view_endpoint, session)
                        view.draw(**SELECTIONS)
                        option = input("> ")
                        view_endpoint = handle_option(option, view, session)
                    case ScreenStates.INSTANCE_DETAILS:
                        view = course_instance_details.CourseInstanceDetailsView()
                        view.request(SERVER_URL + view_endpoint, session)
                        view.draw(**SELECTIONS)
                        option = input("> ")
                        view_endpoint = handle_option(option, view, session)
                    case ScreenStates.REVIEW_LISTING:
                        view = review_listing.ReviewListingView()
                        view.request(SERVER_URL + view_endpoint, session)
                        view.draw(**SELECTIONS)
                        option = input("> ")
                        view_endpoint = handle_option(option, view, session)
                    case ScreenStates.REVIEW_DETAILS:
                        view = review_details.ReviewDetailsView()
                        view.request(SERVER_URL + view_endpoint, session)
                        view.draw(**SELECTIONS)
                        option = input ("> ")
                        view_endpoint = handle_option(option, view, session)
                    case ScreenStates.REVIEW_WRITING:
                        # Only create new instance if not currently writing
                        if view.__class__ != review_writing.ReviewWritingView:
                            view = review_writing.ReviewWritingView()
                        view.request(SERVER_URL + view_endpoint, session)
                        view.draw(**SELECTIONS)
                        option = input("> ")
                        view_endpoint = handle_option(option, view, session)
                    case ScreenStates.REVIEW_EDITING:
                        # Only create new instance if not currently editing
                        if view.__class__ != review_editing.ReviewEditingView:
                            view = review_editing.ReviewEditingView()
                        view.request(SERVER_URL + view_endpoint, session)
                        view.draw(**SELECTIONS)
                        option = input("> ")
                        view_endpoint = handle_option(option, view, session)
                    case ScreenStates.MANUAL:
                        view = manual.ManualView()
                        view.draw()
                        option = input ("> ")
                        _ = handle_option(option, view, session)
                        body = response.json()
                        view_endpoint = body["@controls"]["collection"]["href"]
                    case _:
                        return

            except KeyboardInterrupt:
                return

            except IllegalOption as exception:
                view.draw(**SELECTIONS)
                print(exception.message, exception.option)
                option = input("> ")
                view_endpoint = handle_option(option, view, session)

def handle_option(option: str, view: base_view.View, session: requests.Session):
    """ Handle options for all the screens

    Args:
        option (str): Selected option as string
        view (View): currently active view
        session (Session): Request session for requesting more

    Returns:
        str | None : Path for the next view
    """

    option = option.strip()
    # Make non numeric strings into just one uppercase character
    if not option.isnumeric():
        option = option.upper()[0:1]
    else:
        option = int(option)

    if option == "Q":
        SCREEN_STATE.change_state(ScreenStates.EXIT)
        return None

    match SCREEN_STATE.state:
        case ScreenStates.COURSE_LISTING:
            if option == "H":
                SCREEN_STATE.change_state(ScreenStates.MANUAL)
                return None

            if option in range(len(view.course_list)):
                SELECTIONS["current_course"] = view.course_list[option]
                course_url = view.course_paths[SELECTIONS["current_course"].id]
                response = session.get(SERVER_URL + course_url)
                controls, _ = filter_controls(response.json())
                next_url = controls["feedback:instances"]["href"]
                SCREEN_STATE.change_state(ScreenStates.INSTANCE_LISTING)
                return next_url

            raise IllegalOption(option)

        case ScreenStates.INSTANCE_LISTING:
            if option == "B":
                up_url = view.controls["up"]["href"]
                response = session.get(SERVER_URL + up_url)
                controls, _ = filter_controls(response.json())
                next_url = controls["collection"]["href"]
                SCREEN_STATE.change_state(ScreenStates.COURSE_LISTING)
                return next_url

            if option in range(len(view.course_instance_list)):
                SELECTIONS["current_instance"] = view.course_instance_list[option]
                next_url = view.course_instance_paths[id(SELECTIONS["current_instance"])]
                SCREEN_STATE.change_state(ScreenStates.INSTANCE_DETAILS)
                return next_url

            raise IllegalOption(option)

        case ScreenStates.INSTANCE_DETAILS:
            if option == "A":
                next_url = view.controls["feedback:reviews"]["href"]
                SCREEN_STATE.change_state(ScreenStates.REVIEW_WRITING)
                return next_url

            if option == "L":
                next_url = view.controls["feedback:reviews"]["href"]
                SCREEN_STATE.change_state(ScreenStates.REVIEW_LISTING)
                return next_url

            if option == "B":
                next_url = view.controls["collection"]["href"]
                SCREEN_STATE.change_state(ScreenStates.INSTANCE_LISTING)
                return next_url

            raise IllegalOption(option)

        case ScreenStates.REVIEW_LISTING:
            if option == "B":
                next_url = view.controls["up"]["href"]
                SCREEN_STATE.change_state(ScreenStates.INSTANCE_DETAILS)
                return next_url

            if option in range(len(view.review_list)):
                SELECTIONS["current_review"] = view.review_list[option]
                next_url = view.review_paths[id(SELECTIONS["current_review"])]
                SCREEN_STATE.change_state(ScreenStates.REVIEW_DETAILS)
                return next_url

            raise IllegalOption(option)

        case ScreenStates.REVIEW_DETAILS:
            if option == "L":
                next_url = view.controls["collection"]["href"]
                SCREEN_STATE.change_state(ScreenStates.REVIEW_LISTING)
                return next_url

            if option == "D":
                review_url = view.controls["feedback:delete-review"]["href"]
                response = session.delete(SERVER_URL + review_url)
                next_url = view.controls["collection"]["href"]
                SCREEN_STATE.change_state(ScreenStates.REVIEW_LISTING)
                return next_url

            if option == "E":
                next_url = view.controls["edit"]["href"]
                SCREEN_STATE.change_state(ScreenStates.REVIEW_EDITING)
                return next_url

            raise IllegalOption(option)

        case ScreenStates.REVIEW_WRITING:
            if option == "C":
                next_url = view.controls["up"]["href"]
                SCREEN_STATE.change_state(ScreenStates.INSTANCE_DETAILS)
                return next_url

            if option == "P":
                controls = view.controls["feedback:add-review"]
                method = controls["method"]
                add_url = controls["href"]
                data = view.review.serialize()
                response = session.request(method, SERVER_URL + add_url, json=data)
                next_url = response.headers["Location"]
                view.review.review_date = datetime.date.today()
                SELECTIONS["current_review"] = view.review
                SCREEN_STATE.change_state(ScreenStates.REVIEW_DETAILS)

                return next_url

            if option in view.property_numbers:
                next_url = view.controls["self"]["href"]
                SELECTIONS["current_property_number"] = option
                current_property = view.property_numbers[option]
                property_type = view.properties[current_property]["type"]
                view.editing = True
                view.draw(**SELECTIONS)
                value = input("> ").strip()

                if "null" in property_type and not value:
                    value = None

                if "number" in property_type and value:
                    value = int(value)

                setattr(view.review, current_property, value)

                view.editing = False
                return next_url

            raise IllegalOption(option)

        case ScreenStates.REVIEW_EDITING:
            if option == "C":
                next_url = view.controls["self"]["href"]
                SCREEN_STATE.change_state(ScreenStates.REVIEW_DETAILS)
                return next_url

            if option == "P":
                controls = view.controls["edit"]
                method = controls["method"]
                edit_url = controls["href"]
                data = view.review.serialize()
                response = session.request(method, SERVER_URL + edit_url, json=data)
                next_url = view.controls["self"]["href"]
                SELECTIONS["current_review"] = view.review
                SCREEN_STATE.change_state(ScreenStates.REVIEW_DETAILS)
                return next_url

            if option in view.property_numbers:
                next_url = view.controls["self"]["href"]
                SELECTIONS["current_property_number"] = option
                current_property = view.property_numbers[option]
                property_type = view.properties[current_property]["type"]
                view.editing = True
                view.draw(**SELECTIONS)
                value = input("> ").strip()

                if "null" in property_type and not value:
                    value = None

                if "number" in property_type and value:
                    value = int(value)

                setattr(view.review, current_property, value)

                view.editing = False
                return next_url

            raise IllegalOption(option)

        case ScreenStates.MANUAL:
            if option == "L":
                SCREEN_STATE.change_state(ScreenStates.COURSE_LISTING)
                return None

            raise IllegalOption(option)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--manual',
                        help="open application manual",
                        action='store_true')

    args = parser.parse_args()

    # Screen state machine
    SCREEN_STATE = ScreenStateMachine()

    if args.manual:
        SCREEN_STATE.change_state(ScreenStates.MANUAL)

    SELECTIONS = {
        "current_course": None,
        "current_instance": None,
        "current_review": None,
        "current_property_number": None
    }
    SUCCESS = load_dotenv()
    SERVER_URL = getenv("SERVER_URL", "http://localhost:5000")
    if not SUCCESS:
        print("env file not found")

    main()
