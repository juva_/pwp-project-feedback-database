# PWP SPRING 2023
# Group information
* Student 1. Joona Holappa joona.k.holappa@student.oulu.fi
* Student 2. Akseli Uunila akseli.uunila@student.oulu.fi
* Student 3. Julius Välimaa julius.valimaa@student.oulu.fi

# API: Feedback database
RESTful web API developed using Python Flask framework. Communication is done using MASON and JSON.

## Requirements

We target Python version 3.10

For database we use SQLite version 3.39.4 with SQLAlchemy ORM version 1.4.46

Dependencies are listed in [requirements.txt](requirements.txt)

## Setup

Requirements can be installed by running
```bash
pip install -r requirements.txt
```
After that you should set the environment variable for Flask:

**For Unix-like shells**
```bash
export FLASK_APP=feedback
```
**For Windows cmd**
```cmd
SET FLASK_APP=feedback
```
**For Powershell**
```powershell
$env:FLASK_APP = feedback
```
### Initialising the database

Run `flask init-db` command to create the database:
```bash
flask init-db
```

### Populating the database

Run `flask populate-db` command to create and populate the database with dummy data:
```bash
flask populate-db
```
_Note: This will only create the database if it is not created yet_

## Running the API server

Run `flask run` command to start the server
```bash
flask run
```

By default Flask runs on port `5000` so to connect to the server you can use the URL: `localhost:5000/api/`. This URL also contains the entry point URL of the API which is the course collection resource.

## Running tests

Run `pytest --cov=feedback/` to execute test suite defined in [tests](tests) module

```bash
pytest --cov=feedback/
```

## Documentation
API documentation can be found from `localhost:5000/apidocs/` when the API server is running.

# Client: Feedback CLI
Terminal client for accessing the Feedback database API. Can be used to view, write, edit and delete reviews.

## Requirements

The client is done mostly using pure Python. As with the API server we targeted 3.10 version. We use Requests library version 2.28 for the HTTP communication. We also use python-dotenv version 1.0 for reading the optionally set server URL from an .env file. All the requirements are included in the same [requirements.txt](requirements.txt) file as with the server.

## Setup

Requirements can be installed by running
```bash
pip install -r requirements.txt
```

By default the client connects to `http://localhost:5000` URL
If you want to connect the client to some other URL you can place an `.env` file in the root of the project folder. In the file you should type the `SERVER_URL` key and assign your chosen URL to it.

**For example:**
```bash
#/pwp-project-feedback-database/.env
SERVER_URL=http://localhost:5000
```

## Running the client

Run `python feedback_cli` command to start the client
```bash
python feedback_cli
```
