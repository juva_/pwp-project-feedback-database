# Meetings notes

## Meeting 1.
* **DATE: 2023-02-02**
* **ASSISTANTS: Mika Oja**

### Minutes

Overview seems clear and doable enough.

#### Main concepts and relations

Could be difficult to have enough resources from two concepts.
As one concept results usually in two resources?
However derivative resources might solve that as those can also be read-only.

Derivative resource could be for example rating average for a course.
Something that a separate address can be given to.

#### Api uses
There are multiple ideas on the possible client but one concrete is vague


### Action points
*List here the actions points discussed with assistants*

 * Fill in the resources allocation
 * Consider adding one more concept



## Meeting 2.
* **DATE: 2023-20-02**
* **ASSISTANTS: Iván Sánchez Milara**

### Minutes

The database design and implementation are quite good but some modifications should be done.

Tips on what to use instead of the populate script.

### Action points

Add:
 * Restrict year input (e.g. 2000 - current)
 * Add commands to readme on how to setup and run the project

Update/modify:
 * Wiki documentation (which ORM, what validation done in the API, what in the database)
 * Instead of populate script, use flask add to database with cli

Check:
 * Flask CLI
 * Flask API Project Layout Lovelace

Tips:
 * Check Testing part 2 from Lovelace
 * Test as many cases as possible in testing (e.g. cascade and resource removal)


## Meeting 3.
* **DATE:2023-03-10**
* **ASSISTANTS: Mika Oja**

### Minutes

Overall project is going very good.
Wiki should be a little more comprehensive and code commenting needs polishing.

### Action points

Add:
* Missing docstrings
* Wiki RESTful principles conformity description about PUT and POST

Update/modify:
* Misplaced docstrings


## Meeting 4.
* **DATE:2023-04-21**
* **ASSISTANTS: Iván Sánchez Milara**

### Minutes
Documentation looks good. There were some minor issues.

Hypermedia implementation is good. Wiki documentation needs some work.

Profiles and link relation documentation is good.

Iván mentioned some possible extra work points from the API demonstration script.

### Action points

Add:
* Examples for POST/PUT without the data that is not required.

Fix:
* Broken review item examples. List items are not being displayed correctly

Update/modify:
* Add API entry point into the state diagram and documentation
* Explain reasoning behind custom link relations in the wiki documentation better
* Add missing hours for deadline 4


## Final meeting
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*




